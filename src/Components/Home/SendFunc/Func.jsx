import React, {useState} from 'react';
import {MDBCol, MDBContainer} from "mdbreact";
import {ReactComponent as SubmitButtonIcon} from "feather-icons/dist/icons/log-in.svg";
import styled from "styled-components";
import CustomBackdrop from "../../CustomBackdrop";
import tw from "twin.macro";
import Alert from "./Alert";
const Func = () => {

    const [VIN, setVIN] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();
        event.target.className += " was-validated";

    };

    const handleChange = (event) => {
        setVIN(event.target.value);
        console.log(VIN);
    };

    const postDataHandler = (e) => {
        e.preventDefault();
    };

    let SubmitButton = styled.button`
    background:lavender;
    display:flex;
    justify-content:center;
    align-items: center;
    padding: 1rem 0.5rem;
    border-radius: 5px;
  `;
    const enabled = VIN.length > 16;
    if (enabled) {
        SubmitButton = styled.button`
          background:#000;
    `;
    }
    return (
        <MDBContainer>
            <MDBCol>
                <div>
                    <form className="needs-validation shadow p-5 my-5" onSubmit={handleSubmit} noValidate>
                        <p className="h5 text-center mb4">
                            Enter Your Automobile`s VIN number
                        </p>
                        <label
                            className="grey-text text-left"
                            htmlFor="defaultFormLoginEmailEx">
                            Vehicle Identification Number.
                        </label>
                        <input type="text"
                               value={VIN}
                               name="VIN"
                               id="defaultFormLoginEmailEx"
                               className="form-control"
                               onChange={handleChange}
                               maxLength="17"
                               minLength="17"
                               required
                        />
                        <Alert/>
                        <br/>
                        <div className="d-flex justify-content-center">
                            <SubmitButton
                                type="submit"
                                className="submit-button btn-block text-center"
                                disabled={!enabled}
                                //onClick={postDataHandler}
                            >
                                <SubmitButtonIcon className="icon mr-3"/>
                                <span className="text">What is my car worth</span>
                            </SubmitButton>
                        </div>
                    </form>
                </div>
            </MDBCol>
            {/*<CustomBackdrop/>*/}
        </MDBContainer>
    );
};

export default React.memo(Func);

const FormWrapper = styled.div`
  .submit-button{
    background:{${({enabled}) => (enabled ? 'lavender' : 'lightgreen')};};
  }
`;