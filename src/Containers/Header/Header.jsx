import React, {useState} from 'react';
import {History, Chat, ExitToApp} from "@material-ui/icons";
import "@fortawesome/fontawesome-free/css/all.min.css";

import {
    MDBCollapse, MDBDropdown, MDBDropdownItem, MDBDropdownMenu, MDBDropdownToggle, MDBIcon,
    MDBNavbar,
    MDBTooltip,
    MDBNavbarBrand,
    MDBNavbarNav,
    MDBNavbarToggler,
    MDBNavItem,
    MDBNavLink
} from "mdbreact";

import logo from "../../Assets/companylogo.png";
import styled from "styled-components";
import {Link} from "react-router-dom";

const Header = () => {

    const [collapse, setCollapse] = useState(false);
    //const successLogin =useSelector(successLoginMIne);
    const successLogin = true;

    const toggleCollapse = () => {
        setCollapse(!collapse);
    };

    const token = localStorage.getItem("token");

    const overlay = (
        <div
            id="sidenav-overlay"
            style={{ backgroundColor: "transparent" }}
            onClick={toggleCollapse}
        />
    );

    return (
        <div>
            <HeaderWrapper>
                <MDBNavbar id="navcol" dark expand="md" fixed="top" scrolling>
                    <MDBNavbarBrand href="/" className="py-2 font-weight-bold">
                        <div style={{height: "2.5rem", width: "2.5rem"}}>
                            <img src={logo} alt="qwerty" height="30"/>
                        </div>
                    </MDBNavbarBrand>
                    <MDBNavbarToggler onClick={toggleCollapse}/>
                    <MDBCollapse className="text-left"
                        // id=""
                                 isOpen={collapse}
                                 navbar>
                        <MDBNavbarNav right>
                            <MDBNavItem>
                                <MDBNavLink to="/" onClick={toggleCollapse}>
                                    <strong>Home</strong>
                                </MDBNavLink>
                            </MDBNavItem>
                            <MDBNavItem>
                                <MDBNavLink onClick={toggleCollapse} to="/about">
                                    <strong>About</strong>
                                </MDBNavLink>
                            </MDBNavItem>
                            <MDBNavItem>
                                <MDBNavLink onClick={toggleCollapse} to="/sell">
                                    <strong>Sell Your Car</strong>
                                </MDBNavLink>
                            </MDBNavItem>
                            <MDBNavItem>
                                <MDBNavLink onClick={toggleCollapse} to="/faq">
                                    <strong>FAQ</strong>
                                </MDBNavLink>
                            </MDBNavItem>
                            <MDBNavItem>
                                <MDBNavLink onClick={toggleCollapse} to="/contact">
                                    <strong>Contact</strong>
                                </MDBNavLink>
                            </MDBNavItem>

                            {token || successLogin ? (
                                <MDBNavItem>
                                    <MDBDropdown>
                                        <MDBDropdownToggle nav caret>
                                            <MDBIcon icon="user"/>
                                        </MDBDropdownToggle>
                                        <MDBDropdownMenu className="dropdown-default">
                                            <MDBDropdownItem>
                                                <Link onClick={toggleCollapse} to="/history">
                                                    {" "}<History/>
                                                    <strong> History</strong>
                                                </Link>
                                            </MDBDropdownItem>
                                            <MDBDropdownItem>
                                                <Link onClick={toggleCollapse} to="/chat">
                                                    {" "}<Chat/>
                                                    <strong> Chat</strong>
                                                </Link>
                                            </MDBDropdownItem>
                                            <MDBDropdownItem divider />
                                            <MDBDropdownItem>
                                                <Link onClick={toggleCollapse} to="/">
                                                    {" "}<ExitToApp/>
                                                    <strong> Logout</strong>
                                                </Link>
                                            </MDBDropdownItem>
                                        </MDBDropdownMenu>
                                    </MDBDropdown>
                                </MDBNavItem>
                            ) : (
                                <MDBNavItem>
                                    <MDBNavLink onClick={toggleCollapse} to="/login">
                                        <strong>Login</strong>
                                    </MDBNavLink>
                                </MDBNavItem>
                            )}

                            <div id="ul-navbar">
                                <MDBNavItem>
                                    <MDBTooltip
                                        placement="bottom"
                                        domElement
                                        style={{ display: "block" }}
                                    >
                                        <a
                                            className="nav-link Ripple-parent"
                                            href="https://www.youtube.com/channel/UCyapAPOeRJ-SVktkNszRhvQ?_ga=2.116125317.2081304902.1605710611-609537925.1605092947"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <strong>
                                                <MDBIcon fab icon="youtube" />
                                            </strong>
                                        </a>
                                        <span>YOUTUBE</span>
                                    </MDBTooltip>
                                </MDBNavItem>
                                <MDBNavItem>
                                    <MDBTooltip
                                        placement="bottom"
                                        domElement
                                        style={{ display: "block" }}
                                    >
                                        <a
                                            className="nav-link Ripple-parent"
                                            href="https://www.facebook.com/trustautoinc/"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <strong>
                                                <MDBIcon fab icon="facebook" />
                                            </strong>
                                        </a>
                                        <span>FACEBOOK</span>
                                    </MDBTooltip>
                                </MDBNavItem>
                                <MDBNavItem className="mr-2">
                                    <MDBTooltip
                                        placement="bottom"
                                        domElement
                                        style={{ display: "block" }}
                                    >
                                        <a
                                            className="nav-link Ripple-parent"
                                            href="https://instagram.com/trustautomd"
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            <strong>
                                                <MDBIcon fab icon="instagram" />
                                            </strong>
                                        </a>
                                        <span>INSTAGRAM</span>
                                    </MDBTooltip>
                                </MDBNavItem>
                            </div>
                        </MDBNavbarNav>
                    </MDBCollapse>
                </MDBNavbar>
                {collapse && overlay}
            </HeaderWrapper>

        </div>
    );
};

export default React.memo(Header);

const HeaderWrapper = styled.div`
  #ul-navbar {
  display: flex;
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
  flex-direction: row;
}

.flyout {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
}
#navcol {
  background: linear-gradient(to right, #52006d 0%, #333399 100%);
}
#dropdown {
  color: white;
  box-shadow: none;
}

`;