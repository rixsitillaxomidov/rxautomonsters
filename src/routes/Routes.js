import React, {Suspense, lazy} from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";
import Header from "../Containers/Header/Header";

const Home = lazy(() => import("../Components/Home/Home"));

const Routes = (props) => {
    return (
        <div>
            <Router>
                <Suspense
                    fallback={
                        <>
                            <Header/>
                        </>
                    }
                >
                    <Header/>

                    <Switch>
                        <Route exact path="/">
                            <Home/>
                        </Route>
                    </Switch>
                </Suspense>
            </Router>
        </div>
    );
}

export default React.memo(Routes);